# dw.certbot

Ansible role for installing LetsEncrypt certificates for nginx and other services, or creating fake ones if your server is not live.

Currently made to work with CentOS 8 stream Linux.

## Note

If nginx is already installed when setting up this role (usually the case), you may have remove the SSL cert and key statements from the nginx configuration file. Testing will pass this way and Certbot will add them back, managed.

## Project status

This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.

## Dependencies

This installs nginx as otherwise the cron job is broken, but nginx isn't properly configured.
Probably configure the nginx role first before running this script.

Requires dw.nginx: https://gitlab.com/dustwolf/dw.nginx

On the Ansible machine, install community.crypto:
```
ansible-galaxy collection install community.crypto
```
